# Varnam Languages Data Storage

This repo holds the language data for varnam :
* .vst files - Varnam Symbol Table. This is needed for varnam to understand the basics of a language
* pack files (.vpf) - Pack file is a JSON file that store trained wordings. [Read This](https://github.com/thetronjohnson/varnam/wiki/Making-a-corpus-&-language-pack-for-Varnam)
